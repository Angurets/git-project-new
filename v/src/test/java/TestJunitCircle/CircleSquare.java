package TestJunitCircle;

import FigureSquareCircle.FigureCircle;
import org.junit.Assert;
import org.junit.Test;

public class CircleSquare {

    @Test
    public void figureCircleSquarePositive(){
        FigureCircle figureCircle = new FigureCircle();
        Assert.assertEquals(figureCircle.calcSquare(7), 153);
    }

    @Test(expected = IllegalArgumentException.class)
    public void figureCircleSquareFalse(){
        FigureCircle figureCircle = new FigureCircle();
        figureCircle.calcSquare(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void figureCircleFalseFalse1(){
        FigureCircle figureCircle = new FigureCircle();
        figureCircle.calcSquare(-2);
    }
}
